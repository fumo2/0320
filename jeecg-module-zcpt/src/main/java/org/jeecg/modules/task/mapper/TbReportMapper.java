package org.jeecg.modules.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.task.entity.TbReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: tb_report
 * @Author: jeecg-boot
 * @Date:   2024-02-28
 * @Version: V1.0
 */
public interface TbReportMapper extends BaseMapper<TbReport> {

}
