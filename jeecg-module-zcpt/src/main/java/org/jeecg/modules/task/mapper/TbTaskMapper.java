package org.jeecg.modules.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.task.entity.TbTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: tb_task
 * @Author: jeecg-boot
 * @Date:   2024-02-20
 * @Version: V1.0
 */
public interface TbTaskMapper extends BaseMapper<TbTask> {

}
