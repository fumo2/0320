package org.jeecg.modules.task.service;

import org.jeecg.modules.task.entity.TbTask;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: tb_task
 * @Author: jeecg-boot
 * @Date:   2024-02-20
 * @Version: V1.0
 */
public interface ITbTaskService extends IService<TbTask> {

}
