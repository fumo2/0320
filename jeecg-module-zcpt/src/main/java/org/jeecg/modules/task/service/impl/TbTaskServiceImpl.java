package org.jeecg.modules.task.service.impl;

import org.jeecg.modules.task.entity.TbTask;
import org.jeecg.modules.task.mapper.TbTaskMapper;
import org.jeecg.modules.task.service.ITbTaskService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_task
 * @Author: jeecg-boot
 * @Date:   2024-02-20
 * @Version: V1.0
 */
@Service
public class TbTaskServiceImpl extends ServiceImpl<TbTaskMapper, TbTask> implements ITbTaskService {

}
