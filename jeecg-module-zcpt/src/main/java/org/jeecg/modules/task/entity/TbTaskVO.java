package org.jeecg.modules.task.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author SQW-X
 * @Date 2024/2/21
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class TbTaskVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
    /**
     * 项目ID
     */
    @Excel(name = "项目ID", width = 15)
    @ApiModelProperty(value = "项目ID")
    private String projectId;
    /**
     * 任务名称
     */
    @Excel(name = "任务名称", width = 15)
    @ApiModelProperty(value = "任务名称")
    private String taskName;
    /**
     * 任务描述
     */
    @Excel(name = "任务描述", width = 15)
    @ApiModelProperty(value = "任务描述")
    private String description;
    /**
     * 任务报价
     */
    @Excel(name = "任务报价", width = 15)
    @ApiModelProperty(value = "任务报价")
    private BigDecimal quotation;
    /**
     * 测试类型
     */
    @Excel(name = "测试类型", width = 15)
    @ApiModelProperty(value = "测试类型")
    private String testType;
    /**
     * 需求文档
     */
    @Excel(name = "需求文档", width = 15)
    @ApiModelProperty(value = "需求文档")
    private String document;
    /**
     * 任务截止时间
     */
    @Excel(name = "任务截止时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "任务截止时间")
    private Date endDate;
    @ApiModelProperty(value = "任务创建者")
    private String creator;
    @ApiModelProperty(value = "任务状态")
    private String state;
    private BigDecimal total;
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
    /**
     * 创建日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
    /**
     * 更新日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
}
