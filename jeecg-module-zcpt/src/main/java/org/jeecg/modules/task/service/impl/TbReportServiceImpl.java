package org.jeecg.modules.task.service.impl;

import org.jeecg.modules.task.entity.TbReport;
import org.jeecg.modules.task.mapper.TbReportMapper;
import org.jeecg.modules.task.service.ITbReportService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_report
 * @Author: jeecg-boot
 * @Date:   2024-02-28
 * @Version: V1.0
 */
@Service
public class TbReportServiceImpl extends ServiceImpl<TbReportMapper, TbReport> implements ITbReportService {

}
