package org.jeecg.modules.task.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: tb_report
 * @Author: jeecg-boot
 * @Date:   2024-02-28
 * @Version: V1.0
 */
@Data
@TableName("tb_report")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_report对象", description="tb_report")
public class TbReport implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
	/**任务id*/
	@Excel(name = "任务id", width = 15)
    @ApiModelProperty(value = "任务id")
    private String taskId;
	/**报告名称*/
	@Excel(name = "报告名称", width = 15)
    @ApiModelProperty(value = "报告名称")
    private String reportName;
	/**报告类型*/
	@Excel(name = "报告类型", width = 15)
    @ApiModelProperty(value = "报告类型")
    private String reportType;
	/**测试对象*/
	@Excel(name = "测试对象", width = 15)
    @ApiModelProperty(value = "测试对象")
    private String testObject;
	/**测试内容*/
	@Excel(name = "测试内容", width = 15)
    @ApiModelProperty(value = "测试内容")
    private String testContent;
	/**报告文件*/
	@Excel(name = "报告文件", width = 15)
    @ApiModelProperty(value = "报告文件")
    private String reportFile;
	/**结论*/
	@Excel(name = "结论", width = 15)
    @ApiModelProperty(value = "结论")
    private String conclusion;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}
