package org.jeecg.modules.task.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.task.entity.TbReport;
import org.jeecg.modules.task.service.ITbReportService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: tb_report
 * @Author: jeecg-boot
 * @Date:   2024-02-28
 * @Version: V1.0
 */
@Api(tags="tb_report")
@RestController
@RequestMapping("/task/tbReport")
@Slf4j
public class TbReportController extends JeecgController<TbReport, ITbReportService> {
	@Autowired
	private ITbReportService tbReportService;

	/**
	 * 分页列表查询
	 *
	 * @param tbReport
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "tb_report-分页列表查询")
	@ApiOperation(value="tb_report-分页列表查询", notes="tb_report-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<TbReport>> queryPageList(TbReport tbReport,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @RequestParam(name="taskId",required=false ) String taskId,
								   HttpServletRequest req) {
		QueryWrapper<TbReport> queryWrapper = QueryGenerator.initQueryWrapper(tbReport, req.getParameterMap());
		if (taskId!=null&&taskId!=""){
			queryWrapper.eq("task_id",taskId);
		}
		Page<TbReport> page = new Page<TbReport>(pageNo, pageSize);
		IPage<TbReport> pageList = tbReportService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tbReport
	 * @return
	 */
	@AutoLog(value = "tb_report-添加")
	@ApiOperation(value="tb_report-添加", notes="tb_report-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_report:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody TbReport tbReport) {
		tbReportService.save(tbReport);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tbReport
	 * @return
	 */
	@AutoLog(value = "tb_report-编辑")
	@ApiOperation(value="tb_report-编辑", notes="tb_report-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_report:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody TbReport tbReport) {
		tbReportService.updateById(tbReport);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "tb_report-通过id删除")
	@ApiOperation(value="tb_report-通过id删除", notes="tb_report-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_report:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		tbReportService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "tb_report-批量删除")
	@ApiOperation(value="tb_report-批量删除", notes="tb_report-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_report:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tbReportService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "tb_report-通过id查询")
	@ApiOperation(value="tb_report-通过id查询", notes="tb_report-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<TbReport> queryById(@RequestParam(name="id",required=true) String id) {
		TbReport tbReport = tbReportService.getById(id);
		if(tbReport==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tbReport);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tbReport
    */
    //@RequiresPermissions("org.jeecg.modules.demo:tb_report:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TbReport tbReport) {
        return super.exportXls(request, tbReport, TbReport.class, "tb_report");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_report:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TbReport.class);
    }

}
