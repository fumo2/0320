package org.jeecg.modules.task.service;

import org.jeecg.modules.task.entity.TbReport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: tb_report
 * @Author: jeecg-boot
 * @Date:   2024-02-28
 * @Version: V1.0
 */
public interface ITbReportService extends IService<TbReport> {

}
