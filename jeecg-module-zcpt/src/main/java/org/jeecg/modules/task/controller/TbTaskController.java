package org.jeecg.modules.task.controller;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.poi.hpsf.Decimal;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.project.entity.TbProject;
import org.jeecg.modules.project.service.ITbProjectService;
import org.jeecg.modules.task.entity.TbTask;
import org.jeecg.modules.task.entity.TbTaskVO;
import org.jeecg.modules.task.service.ITbTaskService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.task.entity.TbTask;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: tb_task
 * @Author: jeecg-boot
 * @Date:   2024-02-20
 * @Version: V1.0
 */
@Api(tags="tb_task")
@RestController
@RequestMapping("/task/tbTask")
@Slf4j
@Component
@EnableScheduling
public class TbTaskController extends JeecgController<TbTask, ITbTaskService> {
	@Autowired
	private ITbTaskService tbTaskService;
	@Autowired
	private ITbProjectService tbProjectService;
	/**
	 * 分页列表查询
	 *
	 * @param tbTask
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "tb_task-分页列表查询")
	@ApiOperation(value="tb_task-分页列表查询", notes="tb_task-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<TbTask>> queryPageList(TbTask tbTask,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                   @RequestParam(name="projectId",required=false ) String projectId,
								   HttpServletRequest req) {
		QueryWrapper<TbTask> queryWrapper = QueryGenerator.initQueryWrapper(tbTask, req.getParameterMap());
		if (projectId!=null&&projectId!=""){
		    queryWrapper.eq("project_id",projectId);
        }
		Page<TbTask> page = new Page<TbTask>(pageNo, pageSize);
		IPage<TbTask> pageList = tbTaskService.page(page, queryWrapper);
		return Result.OK(pageList);
	}

     @Scheduled(cron = "0 0 * * * ?")
     public void startTask(){
         LocalDate nowDate =LocalDate.now();
         Instant instant = nowDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
         Date theDate= Date.from(instant);
         LambdaQueryWrapper<TbTask> tbTaskLambdaQueryWrapper=new LambdaQueryWrapper<>();
         List<TbTask> list = tbTaskService.list(tbTaskLambdaQueryWrapper);
         if(list.size()>0){
             for (TbTask tbTask : list) {
                 if (!tbTask.getEndDate().after(theDate)){
                     tbTask.setState(10);
                     tbTaskService.saveOrUpdate(tbTask);
                 }
             }
         }
     }

     //结束任务
	 @GetMapping(value ="finishTask")
	 public Result<?> finishTask(@RequestParam(name="id")String id){
	 LambdaQueryWrapper<TbTask> tbTaskLambdaQueryWrapper=new LambdaQueryWrapper<>();
	 tbTaskLambdaQueryWrapper.eq(TbTask::getId,id);
	 TbTask tbTask=  tbTaskService.getOne(tbTaskLambdaQueryWrapper).setState(2);
	 tbTaskService.saveOrUpdate(tbTask);
	 return Result.ok();
	 }

     public static Date convertLocalDateToDate(LocalDate localDate) {
         // 使用系统默认时区将LocalDate转换为Instant
         Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
         // 使用Date.from()方法将Instant转换为Date
         return Date.from(instant);
     }

	//接收任务
     @GetMapping(value = "/receiveTask")
     public Result<?> receiveTask(@RequestParam(name="recipient") String recipient,
                                     @RequestParam(name="id") String projectId) {
         LambdaQueryWrapper<TbTask> tbTaskLambdaQueryWrapper = new LambdaQueryWrapper<>();
         tbTaskLambdaQueryWrapper.eq(TbTask::getId, projectId);
         TbTask tb=tbTaskService.getOne(tbTaskLambdaQueryWrapper);
         tb.setRecipient(recipient);
         tb.setState(1);
         tbTaskService.saveOrUpdate(tb);
         return Result.OK();
     }


	//查询当前任务的项目剩余的余额
	@GetMapping(value = "/getRestBudget")
	public Result<?> getRestBudget(@RequestParam(name="projectId") String projectId,
                                   @RequestParam(name="taskId",required=false) String taskId){
		LambdaQueryWrapper<TbProject> tbProjectQueryWrapper = new LambdaQueryWrapper();
		tbProjectQueryWrapper.eq(TbProject::getId,projectId);
		TbProject tb=tbProjectService.getOne(tbProjectQueryWrapper);
		BigDecimal total = tb.getBudget();
		LambdaQueryWrapper<TbTask> tbTaskLambdaQueryWrapper = new LambdaQueryWrapper();
		tbTaskLambdaQueryWrapper.eq(TbTask::getProjectId,projectId);
		if (taskId!=null&&taskId!=""){
		    tbTaskLambdaQueryWrapper.ne(TbTask::getId,taskId);
        }
		List<TbTask> tbTaskList=tbTaskService.list(tbTaskLambdaQueryWrapper);
		if (tbTaskList.size()>0){
			for (TbTask tbTask : tbTaskList) {
				total=total.subtract(tbTask.getQuotation());
			}
		}
        TbTaskVO tbTaskVO=new TbTaskVO();
		BeanUtils.copyProperties(tb,tbTaskVO);
        tbTaskVO.setTotal(total);
		return Result.OK(tbTaskVO);
	}
    //根据项目ID出现当前项目下的所有任务信息
//     @GetMapping(value = "/queryTask")
//     public Result<?> queryTask(@RequestParam(name="projectId") String projectId){
//	    LambdaQueryWrapper<TbTask> tbProjectQueryWrapper = new LambdaQueryWrapper();
//	    tbProjectQueryWrapper.eq(TbTask::getProjectId,projectId);
//	    List<TbTask> tbTaskList=tbTaskService.list(tbProjectQueryWrapper);
//	    List<TbTask> tbTaskList2=new ArrayList<>();
//	    if (tbTaskList.size()>0){
//            for (TbTask tbTask : tbTaskList) {
//                tbTask.setProjectId(projectId);
//                tbTaskList2.add(tbTask);
//            }
//        }
//	    return Result.OK(tbTaskList2);
//     }
	/**
	 *   添加
	 *
	 * @param tbTask
	 * @return
	 */
	@AutoLog(value = "tb_task-添加")
	@ApiOperation(value="tb_task-添加", notes="tb_task-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_task:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody TbTask tbTask) {
	    tbTask.setState(0);
		tbTaskService.save(tbTask);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tbTask
	 * @return
	 */
	@AutoLog(value = "tb_task-编辑")
	@ApiOperation(value="tb_task-编辑", notes="tb_task-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_task:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody TbTask tbTask) {
		tbTaskService.updateById(tbTask);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "tb_task-通过id删除")
	@ApiOperation(value="tb_task-通过id删除", notes="tb_task-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_task:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		tbTaskService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "tb_task-批量删除")
	@ApiOperation(value="tb_task-批量删除", notes="tb_task-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_task:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tbTaskService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "tb_task-通过id查询")
	@ApiOperation(value="tb_task-通过id查询", notes="tb_task-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<TbTask> queryById(@RequestParam(name="id",required=true) String id) {
		TbTask tbTask = tbTaskService.getById(id);
		if(tbTask==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tbTask);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tbTask
    */
    //@RequiresPermissions("org.jeecg.modules.demo:tb_task:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TbTask tbTask) {
        return super.exportXls(request, tbTask, TbTask.class, "tb_task");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_task:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TbTask.class);
    }

}
