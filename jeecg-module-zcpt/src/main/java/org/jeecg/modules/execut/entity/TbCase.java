package org.jeecg.modules.execut.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: tb_case
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
@Data
@TableName("tb_case")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_case对象", description="tb_case")
public class TbCase implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
	/**任务id*/
	@Excel(name = "任务id", width = 15)
    @ApiModelProperty(value = "任务id")
    private String taskId;
	/**用例名称*/
	@Excel(name = "用例名称", width = 15)
    @ApiModelProperty(value = "用例名称")
    private String caseName;
	/**用例描述*/
	@Excel(name = "用例描述", width = 15)
    @ApiModelProperty(value = "用例描述")
    private String description;
	/**优先级*/
	@Excel(name = "优先级", width = 15)
    @ApiModelProperty(value = "优先级")
    private String priority;
	/**前置条件*/
	@Excel(name = "前置条件", width = 15)
    @ApiModelProperty(value = "前置条件")
    private String preconditions;
	/**环境配置*/
	@Excel(name = "环境配置", width = 15)
    @ApiModelProperty(value = "环境配置")
    private String environment;
	/**操作步骤*/
	@Excel(name = "操作步骤", width = 15)
    @ApiModelProperty(value = "操作步骤")
    private String step;
	/**输入数据*/
	@Excel(name = "输入数据", width = 15)
    @ApiModelProperty(value = "输入数据")
    private String inputData;
	/**预期结果*/
	@Excel(name = "预期结果", width = 15)
    @ApiModelProperty(value = "预期结果")
    private String expectedResult;
	/**评判标准*/
	@Excel(name = "评判标准", width = 15)
    @ApiModelProperty(value = "评判标准")
    private String standard;
	/**其他说明*/
	@Excel(name = "其他说明", width = 15)
    @ApiModelProperty(value = "其他说明")
    private String others;
	/**测试结果*/
	@Excel(name = "测试结果", width = 15)
    @ApiModelProperty(value = "测试结果")
    private String testResult;
	/**测试结论*/
	@Excel(name = "测试结论", width = 15)
    @ApiModelProperty(value = "测试结论")
    private String conclusion;
	/**附件*/
	@Excel(name = "附件", width = 15)
    @ApiModelProperty(value = "附件")
    private String annex;
	/**截图*/
	@Excel(name = "截图", width = 15)
    @ApiModelProperty(value = "截图")
    private String screenshot;
	/**创建人名称*/
    @ApiModelProperty(value = "创建人名称")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人名称*/
    @ApiModelProperty(value = "更新人名称")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
}
