package org.jeecg.modules.execut.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.execut.entity.TbCase;
import org.jeecg.modules.execut.service.ITbCaseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.execut.entity.TbCase;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: tb_case
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
@Api(tags="tb_case")
@RestController
@RequestMapping("/execut/tbCase")
@Slf4j
public class TbCaseController extends JeecgController<TbCase, ITbCaseService> {
	@Autowired
	private ITbCaseService tbCaseService;
	
	/**
	 * 分页列表查询
	 *
	 * @param tbCase
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "tb_case-分页列表查询")
	@ApiOperation(value="tb_case-分页列表查询", notes="tb_case-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<TbCase>> queryPageList(TbCase tbCase,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									@RequestParam(name="taskId", required = false) String taskId,
								   HttpServletRequest req) {
		QueryWrapper<TbCase> queryWrapper = QueryGenerator.initQueryWrapper(tbCase, req.getParameterMap());
		if (taskId!=null&&taskId!=""){
			queryWrapper.eq("task_id",taskId);
		}
		Page<TbCase> page = new Page<TbCase>(pageNo, pageSize);
		IPage<TbCase> pageList = tbCaseService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param tbCase
	 * @return
	 */
	@AutoLog(value = "tb_case-添加")
	@ApiOperation(value="tb_case-添加", notes="tb_case-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_case:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody TbCase tbCase) {
		tbCaseService.save(tbCase);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tbCase
	 * @return
	 */
	@AutoLog(value = "tb_case-编辑")
	@ApiOperation(value="tb_case-编辑", notes="tb_case-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_case:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody TbCase tbCase) {
		tbCaseService.updateById(tbCase);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "tb_case-通过id删除")
	@ApiOperation(value="tb_case-通过id删除", notes="tb_case-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_case:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		tbCaseService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "tb_case-批量删除")
	@ApiOperation(value="tb_case-批量删除", notes="tb_case-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_case:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tbCaseService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "tb_case-通过id查询")
	@ApiOperation(value="tb_case-通过id查询", notes="tb_case-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<TbCase> queryById(@RequestParam(name="id",required=true) String id) {
		TbCase tbCase = tbCaseService.getById(id);
		if(tbCase==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tbCase);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tbCase
    */
    //@RequiresPermissions("org.jeecg.modules.demo:tb_case:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TbCase tbCase) {
        return super.exportXls(request, tbCase, TbCase.class, "tb_case");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_case:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TbCase.class);
    }

}
