package org.jeecg.modules.execut.service;

import org.jeecg.modules.execut.entity.TbCase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: tb_case
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
public interface ITbCaseService extends IService<TbCase> {

}
