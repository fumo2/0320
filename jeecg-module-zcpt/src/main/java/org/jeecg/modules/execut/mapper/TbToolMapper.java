package org.jeecg.modules.execut.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.execut.entity.TbTool;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: tb_tool
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
public interface TbToolMapper extends BaseMapper<TbTool> {

}
