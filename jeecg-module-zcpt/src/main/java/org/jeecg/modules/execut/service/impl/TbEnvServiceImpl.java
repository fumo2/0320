package org.jeecg.modules.execut.service.impl;

import org.jeecg.modules.execut.entity.TbEnv;
import org.jeecg.modules.execut.mapper.TbEnvMapper;
import org.jeecg.modules.execut.service.ITbEnvService;
import org.jeecg.modules.execut.entity.TbEnv;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_env
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
@Service
public class TbEnvServiceImpl extends ServiceImpl<TbEnvMapper, TbEnv> implements ITbEnvService {

}
