package org.jeecg.modules.execut.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.execut.entity.TbDefect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: tb_defect
 * @Author: jeecg-boot
 * @Date:   2024-02-29
 * @Version: V1.0
 */
public interface TbDefectMapper extends BaseMapper<TbDefect> {

}
