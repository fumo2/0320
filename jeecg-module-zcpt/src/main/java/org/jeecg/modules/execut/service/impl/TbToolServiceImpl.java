package org.jeecg.modules.execut.service.impl;

import org.jeecg.modules.execut.entity.TbTool;
import org.jeecg.modules.execut.mapper.TbToolMapper;
import org.jeecg.modules.execut.service.ITbToolService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_tool
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
@Service
public class TbToolServiceImpl extends ServiceImpl<TbToolMapper, TbTool> implements ITbToolService {

}
