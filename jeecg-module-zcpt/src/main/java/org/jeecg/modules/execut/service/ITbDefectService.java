package org.jeecg.modules.execut.service;

import org.jeecg.modules.execut.entity.TbDefect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: tb_defect
 * @Author: jeecg-boot
 * @Date:   2024-02-29
 * @Version: V1.0
 */
public interface ITbDefectService extends IService<TbDefect> {

}
