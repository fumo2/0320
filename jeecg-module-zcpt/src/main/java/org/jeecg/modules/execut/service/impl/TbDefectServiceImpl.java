package org.jeecg.modules.execut.service.impl;

import org.jeecg.modules.execut.entity.TbDefect;
import org.jeecg.modules.execut.mapper.TbDefectMapper;
import org.jeecg.modules.execut.service.ITbDefectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_defect
 * @Author: jeecg-boot
 * @Date:   2024-02-29
 * @Version: V1.0
 */
@Service
public class TbDefectServiceImpl extends ServiceImpl<TbDefectMapper, TbDefect> implements ITbDefectService {

}
