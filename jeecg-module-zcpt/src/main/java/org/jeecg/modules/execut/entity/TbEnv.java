package org.jeecg.modules.execut.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: tb_env
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
@Data
@TableName("tb_env")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_env对象", description="tb_env")
public class TbEnv implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
	/**任务id*/
	@Excel(name = "任务id", width = 15)
    @ApiModelProperty(value = "任务id")
    private String taskId;
	/**设备名字*/
	@Excel(name = "设备名字", width = 15)
    @ApiModelProperty(value = "设备名字")
    private String equipmentName;
	/**设备型号*/
	@Excel(name = "设备型号", width = 15)
    @ApiModelProperty(value = "设备型号")
    private String equipmentModel;
	/**设备数量*/
	@Excel(name = "设备数量", width = 15)
    @ApiModelProperty(value = "设备数量")
    private Integer equipmentNumber;
	/**操作系统*/
	@Excel(name = "操作系统", width = 15)
    @ApiModelProperty(value = "操作系统")
    private String operatingSystem;
	/**CPU数量*/
	@Excel(name = "CPU数量", width = 15)
    @ApiModelProperty(value = "CPU数量")
    private Integer cpu;
	/**内存*/
	@Excel(name = "内存", width = 15)
    @ApiModelProperty(value = "内存")
    private Integer memory;
	/**硬盘*/
	@Excel(name = "硬盘", width = 15)
    @ApiModelProperty(value = "硬盘")
    private Integer disk;
	/**中间件*/
	@Excel(name = "中间件", width = 15)
    @ApiModelProperty(value = "中间件")
    private String middleware;
	/**数据库*/
	@Excel(name = "数据库", width = 15)
    @ApiModelProperty(value = "数据库")
    private String base;
	/**浏览器*/
	@Excel(name = "浏览器", width = 15)
    @ApiModelProperty(value = "浏览器")
    private String browser;
    /**创建人名称*/
    @ApiModelProperty(value = "创建人名称")
    private String createBy;
    /**创建日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
    /**更新人名称*/
    @ApiModelProperty(value = "更新人名称")
    private String updateBy;
    /**更新日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
}
