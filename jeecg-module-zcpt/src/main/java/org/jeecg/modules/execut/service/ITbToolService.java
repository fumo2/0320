package org.jeecg.modules.execut.service;

import org.jeecg.modules.execut.entity.TbTool;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: tb_tool
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
public interface ITbToolService extends IService<TbTool> {

}
