package org.jeecg.modules.execut.service.impl;

import org.jeecg.modules.execut.entity.TbCase;
import org.jeecg.modules.execut.mapper.TbCaseMapper;
import org.jeecg.modules.execut.service.ITbCaseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: tb_case
 * @Author: jeecg-boot
 * @Date:   2024-02-23
 * @Version: V1.0
 */
@Service
public class TbCaseServiceImpl extends ServiceImpl<TbCaseMapper, TbCase> implements ITbCaseService {

}
