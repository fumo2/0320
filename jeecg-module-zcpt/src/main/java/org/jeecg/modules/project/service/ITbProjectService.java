package org.jeecg.modules.project.service;

import org.jeecg.modules.project.entity.TbProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目信息表
 * @Author: jeecg-boot
 * @Date:   2023-12-25
 * @Version: V1.0
 */
public interface ITbProjectService extends IService<TbProject> {

}
