package org.jeecg.modules.project.service.impl;

import org.jeecg.modules.project.entity.TbProject;
import org.jeecg.modules.project.mapper.TbProjectMapper;
import org.jeecg.modules.project.service.ITbProjectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目信息表
 * @Author: jeecg-boot
 * @Date:   2023-12-25
 * @Version: V1.0
 */
@Service
public class TbProjectServiceImpl extends ServiceImpl<TbProjectMapper, TbProject> implements ITbProjectService {

}
