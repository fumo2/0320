package org.jeecg.modules.project.mapper;

import org.jeecg.modules.project.entity.TbProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目信息表
 * @Author: jeecg-boot
 * @Date:   2023-12-25
 * @Version: V1.0
 */
public interface TbProjectMapper extends BaseMapper<TbProject> {

}
