package org.jeecg.modules.project.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 项目信息表
 * @Author: jeecg-boot
 * @Date:   2023-12-25
 * @Version: V1.0
 */
@Data
@TableName("tb_project")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="tb_project对象", description="项目信息表")
public class TbProject implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**项目标题*/
	@Excel(name = "项目标题", width = 15)
    @ApiModelProperty(value = "项目标题")
    private java.lang.String title;
	/**项目状态*/
	@Excel(name = "项目状态", width = 15)
    @ApiModelProperty(value = "项目状态")
    private java.lang.Integer state;
	/**联系人*/
	@Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String contactor;
	/**联系人*/
	@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String tel;
	/**预算*/
	@Excel(name = "预算", width = 15)
    @ApiModelProperty(value = "预算")
    private BigDecimal budget;
	/**领域类型*/
	@Excel(name = "领域类型", width = 15)
    @ApiModelProperty(value = "领域类型")
    private java.lang.String domainType;
	/**应用类型*/
	@Excel(name = "应用类型", width = 15)
    @ApiModelProperty(value = "应用类型")
    private java.lang.String appType;
	/**测试类型*/
	@Excel(name = "测试类型", width = 15)
    @ApiModelProperty(value = "测试类型")
    private java.lang.String testType;
	/**项目可见性*/
	@Excel(name = "项目可见性", width = 15)
    @ApiModelProperty(value = "项目可见性")
    private java.lang.String visibleType;
	/**项目可见市*/
	@Excel(name = "项目可见公司", width = 15)
    @ApiModelProperty(value = "项目可见市")
    private java.lang.String visibleUnits;
	/**需求文档*/
	@Excel(name = "需求文档", width = 15)
    @ApiModelProperty(value = "需求文档")
    private java.lang.String requireDoc;
	/**安装包*/
	@Excel(name = "安装包", width = 15)
    @ApiModelProperty(value = "安装包")
    private java.lang.String installFile;
	/**项目截止时间*/
	@Excel(name = "项目截止时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "项目截止时间")
    private java.util.Date endDate;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
    @ApiModelProperty(value = "项目创建者")
    private java.lang.String creator;
    @ApiModelProperty(value = "项目接收者")
    private java.lang.String recipient;

}
