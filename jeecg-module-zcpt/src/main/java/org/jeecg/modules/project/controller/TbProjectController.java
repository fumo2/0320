package org.jeecg.modules.project.controller;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.tomcat.jni.LibraryNotFoundError;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.project.entity.TbProject;
import org.jeecg.modules.project.service.ITbProjectService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.task.entity.TbTask;
import org.jeecg.modules.task.service.ITbTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 项目信息表
 * @Author: jeecg-boot
 * @Date:   2023-12-25
 * @Version: V1.0
 */
@Api(tags="项目信息表")
@RestController
@RequestMapping("/project/tbProject")
@Slf4j
@Component
@EnableScheduling
public class TbProjectController extends JeecgController<TbProject, ITbProjectService> {
	@Autowired
	private ITbProjectService tbProjectService;
	@Autowired
	private ITbTaskService tbTaskService;
	/**
	 * 分页列表查询
	 *
	 * @param tbProject
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "项目信息表-分页列表查询")
	@ApiOperation(value="项目信息表-分页列表查询", notes="项目信息表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<TbProject>> queryPageList(TbProject tbProject,
												  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
												  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
												  @RequestParam(name = "userId") String userId,
												  HttpServletRequest req) {
		QueryWrapper<TbProject> queryWrapper = QueryGenerator.initQueryWrapper(tbProject, req.getParameterMap());
		queryWrapper.and(wrapper->wrapper.eq("state",0).or().eq("creator",userId).or().eq("recipient",userId));
		Page<TbProject> page = new Page<TbProject>(pageNo, pageSize);
		IPage<TbProject> pageList = tbProjectService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	 @Scheduled(cron = "0 0 * * * ?")
	 public void startTask(){
		 LocalDate nowDate =LocalDate.now();
		 LambdaQueryWrapper<TbProject> tbProjectLambdaQueryWrapper=new LambdaQueryWrapper<>();
		 List<TbProject> list = tbProjectService.list(tbProjectLambdaQueryWrapper);
		 if(list.size()>0){
			 for (TbProject tbProject : list) {
				 if (!tbProject.getEndDate().after(convertLocalDateToDate(nowDate))){
                  tbProject.setState(10);
                  tbProjectService.saveOrUpdate(tbProject);
				 }
			 }
		 }
	 }

	 public static Date convertLocalDateToDate(LocalDate localDate) {
		 // 使用系统默认时区将LocalDate转换为Instant
		 Instant instant = localDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
		 // 使用Date.from()方法将Instant转换为Date
		 return Date.from(instant);
	 }

	 //结束项目
	 //进行中和未接收的任务状态变为已截止
	 @GetMapping(value ="finishProject")
	 public Result<?> finishProject(@RequestParam(name="id")String id){
		 LambdaQueryWrapper<TbProject> tbTaskLambdaQueryWrapper=new LambdaQueryWrapper<>();
		 tbTaskLambdaQueryWrapper.eq(TbProject::getId,id);
		 TbProject tbProject=  tbProjectService.getOne(tbTaskLambdaQueryWrapper).setState(2);
		 tbProjectService.saveOrUpdate(tbProject);
		 LambdaQueryWrapper<TbTask> tbTaskLambdaQueryWrapper1=new LambdaQueryWrapper<>();
		 tbTaskLambdaQueryWrapper1.eq(TbTask::getProjectId,id);
		 List<TbTask> tbTaskList=tbTaskService.list(tbTaskLambdaQueryWrapper1);
		 if (tbTaskList.size()>0){
			 for (TbTask tbTask : tbTaskList) {
				 if (tbTask.getState()==0||tbTask.getState()==1){
				 	tbTask.setState(10);
				 	tbTaskService.saveOrUpdate(tbTask);
				 }
			 }
		 }
		 return Result.ok();
	 }
	//接收项目
	@GetMapping(value = "/receiveProject")
	public Result<?> receiveProject(@RequestParam(name="recipient") String recipient,
									@RequestParam(name="id") String projectId) {
		LambdaQueryWrapper<TbProject> tbProjectQueryWrapper = new LambdaQueryWrapper<>();
		tbProjectQueryWrapper.eq(TbProject::getId, projectId);
		TbProject tb=tbProjectService.getOne(tbProjectQueryWrapper);
		tb.setRecipient(recipient);
		tb.setState(1);
		tbProjectService.saveOrUpdate( tb);
		return Result.OK();
	}

	/**
	 *   添加
	 *
	 * @param tbProject
	 * @return
	 */
	@AutoLog(value = "项目信息表-添加")
	@ApiOperation(value="项目信息表-添加", notes="项目信息表-添加")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_project:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody TbProject tbProject) {
		tbProject.setState(0);
		tbProjectService.save(tbProject);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param tbProject
	 * @return
	 */
	@AutoLog(value = "项目信息表-编辑")
	@ApiOperation(value="项目信息表-编辑", notes="项目信息表-编辑")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_project:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody TbProject tbProject) {
		tbProject.setState(0);
		tbProjectService.updateById(tbProject);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "项目信息表-通过id删除")
	@ApiOperation(value="项目信息表-通过id删除", notes="项目信息表-通过id删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_project:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		tbProjectService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "项目信息表-批量删除")
	@ApiOperation(value="项目信息表-批量删除", notes="项目信息表-批量删除")
	//@RequiresPermissions("org.jeecg.modules.demo:tb_project:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.tbProjectService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "项目信息表-通过id查询")
	@ApiOperation(value="项目信息表-通过id查询", notes="项目信息表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<TbProject> queryById(@RequestParam(name="id",required=true) String id) {
		TbProject tbProject = tbProjectService.getById(id);
		if(tbProject==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(tbProject);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param tbProject
    */
    //@RequiresPermissions("org.jeecg.modules.demo:tb_project:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TbProject tbProject) {
        return super.exportXls(request, tbProject, TbProject.class, "项目信息表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("tb_project:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TbProject.class);
    }

}
